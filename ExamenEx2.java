import java.util.Scanner;
public class ExamenEx2 {
    public static void main(String[] args) {
        Scanner ip = new Scanner(System.in);
        System.out.print("Enter the number of the day: ");
        int day = ip.nextInt();


        switch (day){
            case 1:
                System.out.println("It's monday");
                break;
            case 2:
                System.out.println("It's tuesday");
                break;
            case 3:
                System.out.println("It's wednesday");
                break;
            case 4:
                System.out.println("It's thursday");
                break;
            case 5:
                System.out.println("It's friday");
                break;
            case 6:
                System.out.println("It's saturday");
                break;
            case 7:
                System.out.println("It's sunday");
                break;
        }
    }
}
